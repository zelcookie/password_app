import torch
import os
from tqdm import tqdm
from model.utils import RMSLELoss
from model.dataset import collate_fn, RnnDataset
from torch.utils.data import DataLoader, RandomSampler


class RnnModel(torch.nn.Module):
    def __init__(self, config):
        super(RnnModel, self).__init__()
        self.embeddings = torch.nn.Embedding(config['vocab_size'], config['embedding_dim'])
        self.rnn = torch.nn.LSTM(config['embedding_dim'], config['rnn_dim'], batch_first=True,
                                 dropout=config['rnn_dropout'], num_layers=config['n_rnn_layers'],
                                 bidirectional=config['bidirectional'])
        self.linear = torch.nn.Linear(config['rnn_dim'], 1)
        self.relu = torch.nn.ReLU()

    def forward(self, sequences):
        embeddings = self.embeddings(sequences)
        output, (h_n, c_n) = self.rnn(embeddings)
        result = self.linear(h_n)
        result = self.relu(result)

        return result.squeeze()


class Model:
    def __init__(self, config, checkpoint_path):
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.checkpoint_path = checkpoint_path
        self.config = config
        self.model = None
        self.char2ind = None

    def get_checkpoint(self):
        checkpoint = dict()
        checkpoint['config'] = self.config
        checkpoint['char2ind'] = self.char2ind
        checkpoint['state_dict'] = self.model.state_dict()
        return checkpoint

    def save_model(self):
        with open(self.checkpoint_path, 'wb') as f:
            torch.save(self.get_checkpoint(), f)

    def load_model(self):
        print(os.path.abspath(__file__))
        with open(self.checkpoint_path, 'rb') as f:
            checkpoint = torch.load(f, map_location=torch.device(self.device))
        self.config = checkpoint['config']
        self.char2ind = checkpoint['char2ind']
        self.model = RnnModel(self.config)
        self.model.load_state_dict(checkpoint['state_dict'])
        if self.device == 'cuda':
            self.model = self.model.cuda()

    def train_epoch(self, iterator, optimizer, criterion):
        self.model.train()
        epoch_loss = 0
        for i, batch in tqdm(enumerate(iterator)):
            inputs, targets = batch
            inputs = inputs.to(self.device)
            targets = targets.to(self.device)

            optimizer.zero_grad()

            preds = self.model(inputs)
            loss = criterion(preds, targets)
            loss.backward()
            torch.nn.utils.clip_grad_norm_(self.model.parameters(), 1)
            optimizer.step()

            epoch_loss += loss.item()

        return epoch_loss / len(iterator)

    def validate_epoch(self, iterator, criterion):
        self.model.eval()
        epoch_loss = 0
        predictions = []
        with torch.no_grad():
            for i, batch in tqdm(enumerate(iterator)):
                inputs, targets = batch
                inputs = inputs.to(self.device)
                targets = targets.to(self.device)

                preds = self.model(inputs)
                loss = criterion(preds, targets)
                predictions += preds.tolist()
                epoch_loss += loss.item()

        return epoch_loss / len(iterator), predictions

    def fit(self, df_train, df_val,  epochs=10, batch_size=256):
        self.char2ind = {char: ind+1 for ind, char in enumerate(set(''.join(df_train.Password.tolist())))}
        self.config['vocab_size'] = len(self.char2ind) + 1,
        self.model = RnnModel(self.config)
        train_dataset = RnnDataset(df_train.Password, df_train.Times.tolist(), self.char2ind)
        train_sampler = RandomSampler(train_dataset)
        train_dataloader = DataLoader(train_dataset, batch_size=batch_size, collate_fn=collate_fn,
                                      sampler=train_sampler)
        val_dataset = RnnDataset(df_val.Password, df_val.Times.tolist(), self.char2ind)
        val_dataloader = DataLoader(val_dataset, batch_size=batch_size, collate_fn=collate_fn)

        criterion = RMSLELoss()
        optimizer = torch.optim.Adam(self.model.parameters(), lr=0.001)
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=3, gamma=0.5, last_epoch=-1)

        best_valid_loss = float('inf')
        for epoch in range(1, epochs + 1):
            train_loss = self.train_epoch(train_dataloader, optimizer, criterion)
            valid_loss, predictions = self.validate_epoch(val_dataloader, criterion)
            print(round(train_loss, 5), round(valid_loss, 5))
            valid_loss = round(valid_loss, 4)
            if valid_loss < best_valid_loss:
                best_valid_loss = valid_loss
                self.save_model()

            scheduler.step()

    def predict(self, passwords):
        if self.model is None:
            self.load_model()
        if type(passwords) == str:
            passwords = [passwords]
        test_dataset = RnnDataset(passwords, [1]*len(passwords), self.char2ind)
        test_dataloader = DataLoader(test_dataset, batch_size=256, collate_fn=collate_fn)
        self.model.eval()
        predictions = []
        with torch.no_grad():
            for i, batch in tqdm(enumerate(test_dataloader)):
                inputs, targets = batch
                if len(passwords) == 1:
                    padded = torch.zeros(1, max(8, inputs.shape[1]), dtype=torch.long)
                    padded[:, :inputs.shape[1]] = inputs
                    inputs = padded
                inputs = inputs.to(self.device)
                preds = self.model(inputs).tolist()
                if type(preds) == float:
                    predictions.append(preds)
                else:
                    predictions += preds
        predictions = [pred if pred > 1 else 1 for pred in predictions]
        return predictions if len(passwords) > 1 else predictions[0]
