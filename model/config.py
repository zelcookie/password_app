config = {
    'embedding_dim': 64,
    'rnn_dim': 256,
    'rnn_dropout': 0.1,
    'n_rnn_layers': 1,
    'bidirectional': False,
    'file_path': './checkpoints/result_model.pt'
}