import torch
from torch.utils.data import Dataset
from torch.nn.utils.rnn import pad_sequence

def collate_fn(batch):
    sequences = [sample['input'] for sample in batch]
    targets = torch.tensor([sample['target'] for sample in batch], dtype=torch.float)
    paded_sequences = pad_sequence(sequences, batch_first=True)
    return paded_sequences, targets


class RnnDataset(Dataset):
    def __init__(self, texts, target, char2ind):
        self.target = torch.tensor(target)
        self.texts = texts
        self.char2ind = char2ind
        self.processed_texts = self.process_texts(texts)

    def __getitem__(self, index):
        return {
            'target': self.target[index],
            'input': torch.tensor(self.processed_texts[index])
        }

    def __len__(self):
        return len(self.texts)

    def process(self, text):
        return [self.char2ind[char] for char in text if char in self.char2ind]


    def process_texts(self, texts):
        return [self.process(text) for text in texts]