from flask import Flask, request, render_template
from model.model import Model
from model.config import config
app = Flask(__name__)

def password_model(pw):
    return 10
model = Model(config, './model/checkpoints/result_model.pt')

@app.route("/", methods=["POST", "GET"])
def index():
    """Main form rendering"""
    if request.method == 'POST':
        pw = request.form["password"]
        pass_freq = model.predict(pw) if pw.strip() != '' else 'EMPTY_INPUT'
        return render_template("index.html", password=pw, prediction=pass_freq)
    else:
        return render_template("index.html")


if __name__ == '__main__':
    app.run(debug=True)